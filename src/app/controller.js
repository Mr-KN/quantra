/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 * 
 */
;
(function() {

  angular
    .module('quantraApp')
    .controller('MainController', MainController);

  MainController.$inject = ['QueryService', '$timeout'];


  function MainController(QueryService, $timeout) {

    // 'controller as' syntax
    var self = this;
    var courseSwiper;
    self.courses = [];


    $timeout(function() {

      courseSwiper = new Swiper('.course-details .swiper-container', {
        // Optional parameters
        loop: true,

        pagination: {
          el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },


      })
    });


    /**
     * Load some data
     * @return {Object} Returned object
     */

    for (var i = 1; i <= 3; i++) {
      QueryService.query('GET', `course${i}.json`, {}, {})
        .then(function(data) {
          self.courses.push(data.data.result);
          console.log(self.courses);
        });
    }

  }


})();
