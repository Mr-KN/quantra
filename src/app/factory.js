;(function() {


  /**
   * Sample factory
   *
   * You can fetch here some data from API and the use them
   * in controller
   * 
   */
  angular
    .module('quantraApp')
    .factory('getDataFromAPI', getDataFromAPI);

  getDataFromAPI.$inject = ['$http'];


  ////////////


  function getDataFromAPI($http) {

    return {
      loadData: loadData
    };


  }


})();
